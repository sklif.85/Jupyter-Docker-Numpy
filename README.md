Jupyter Data Science Stack + Docker-compose

0. Install docker-compose

1. Clone repo
    - git clone https://gitlab.com/sklif.85/Jupyter-Docker-Numpy.git && cd Jupyter-Docker-Numpy

2. Building new docker service with Jupyter server:
    - sh run.sh

3. Clean all what you create with previous command
    - sh clean.sh
